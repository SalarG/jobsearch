import Expo, { Notifications } from 'expo';
import React from 'react';
import { StyleSheet, Text, View, Animated, Alert } from 'react-native';
import {
  createBottomTabNavigator,
  createAppContainer,
  createStackNavigator
} from 'react-navigation';
import { Provider } from 'react-redux';
import registerForNotifications from './services/push_notifications'
import store from './store/index'
import AuthScreen from './screens/AuthScreen';
import WelcomeScreen from './screens/WelcomScreen';
import MapScreen from './screens/MapScreen';
import DeckScreen from './screens/DeckScreen';
import ReviewScreen from './screens/ReviewScreen';
import SettingScreen from './screens/SettingScreen';



export default class App extends React.Component {

  componentDidMount() {
    registerForNotifications();

    Notifications.addListener((notification) => {
      const { data: { text }, origin } = notification;
      if (origin === 'received' && text) {
        Alert.alert(
          'New Push Notification',
          text,
          [{ text: 'OK.' }]
        )
      }
    })
  }

  render() {
    const MainNavigator = createBottomTabNavigator({
      welcome: { screen: WelcomeScreen },
      auth: { screen: AuthScreen },
      main: {
        screen: createBottomTabNavigator({
          map: { screen: MapScreen },
          deck: { screen: DeckScreen },
          review: {
            screen: createStackNavigator({
              review: { screen: ReviewScreen },
              settings: { screen: SettingScreen }
            })
          }
        }, {
            tabBarPosition: 'bottom',
            tabBarOptions: {
              labelStyle: { fontSize: 12 }
            }
          }
        )
      }
    },
      {
        navigationOptions: {
          tabBarVisible: false
        },
        lazy: true,

      }
    );
    const App = createAppContainer(MainNavigator);

    return (
      <Provider store={store}>
        <Animated.View style={styles.container}>
          <App />
        </Animated.View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',

    backgroundColor: '#fff',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 30,
    marginBottom: 10,
    borderWidth: 1,
    // borderBottomWidth: 1

  },
  main: {
    flex: 1

  }
});
