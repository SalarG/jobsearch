import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';
import {
    FACEBOOK_LOGIN_SUCCESS,
    FACEBOOK_LOGIN_FAIL
} from './types';

//How to use it
//AsynchStorage.setItem('fb_token', token)
//AsynchStorage.getItem('fb_token'); 

export const facebookLogin = () => async dispatch => {

    let token = await AsyncStorage.getItem('fb_token');
    if (token) {
        //Dispatch an action saying FB login is doen
        dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token })
        console.log('token 1', token )
    } else {
        //Start up FB Login process
        doFacebookLogin(dispatch);
     
    }
};

const doFacebookLogin = async dispatch => {

    let { token, type } = await Facebook.logInWithReadPermissionsAsync('1902368523149645', {
        permissions: ['public_profile']
    });
   

    if (type === 'cancel') {
        return dispatch({ type: FACEBOOK_LOGIN_FAIL })
    }

    await AsyncStorage.setItem('fb_token', token);
    dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
};