import { Permissions, Notifications } from 'expo';
import axios from 'axios';

const PUSH_ENDPOINT = 'http://rallycoding.herokuapp.com/api/tokens';

export default async () => {
    let previousToken = await AsynncStorage.getItem('pushtoken');
    if (previousToken) {
        return;
    } else {
        let { status } = await Permissions.askAsync(Permissions.REMOTE_NOTIFICAIONS);

        if (status !== 'granted') {
            return;
        }
        let token = await Notifications.getExpoPushTokenAsync();
        axios.post(PUSH_ENDPOINT, { token: { token } });
        AsynncStorage.setItem('pushtoken', token);
    }
};
